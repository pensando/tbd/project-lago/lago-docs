***************
Compute Servers
***************

For the Linux compute servers, it is recommended to use Ubuntu (18.04 or later) for demo/poc setups.
If there is an existing k8s cluster or are more comfortable using a different OS, e.g. CentOS, that can be done as well.

It is also recommended to use a separate system as an "admin" system.  This means using a laptop
or a different server/VM to run all of the kubernetes and kafka commands from.  It's not required, but installing the
admin tools on a cluster node usually isn't recommended.  This is easily done using brew on MacOSX, and package managers
on Linux even if it is running under WSL2 in Windows

.. note::
   The **Number of Nodes** in the tables below refers to the number of Kubernetes nodes in the cluster

**Use this table if to deploy a Kafka cluster:**

+-----------------+-------+-------+--------+------------+
| Number of Nodes |  RAM  |  HDD  |  vCPU  | # of DSCs  |
+=================+=======+=======+========+============+
|         1       |  16GB | 100GB |    2   |     10     |
+-----------------+-------+-------+--------+------------+
|         1       |  32GB | 200GB |    4   |      50    |
+-----------------+-------+-------+--------+------------+
|         3       |  16GB | 200GB |    2   |      100   |
+-----------------+-------+-------+--------+------------+
|         3       |  24GB | 500GB |    4   |     10000  |
+-----------------+-------+-------+--------+------------+
|         3       |  32GB | 1TB   |    8   |     30000  |
+-----------------+-------+-------+--------+------------+


**Use this table if there is an existing Kafka cluster that the producers can push to:**

+-----------------+-------+-------+--------+------------+
| Number of Nodes |  RAM  |  HDD  |  vCPU  | # of DSCs  |
+=================+=======+=======+========+============+
|         1       |  16GB | 100GB |    2   |     100    |
+-----------------+-------+-------+--------+------------+
|         3       |  16GB | 100GB |    4   |    10000   |
+-----------------+-------+-------+--------+------------+
|         3       |  24GB | 200GB |    8   |    30000   |
+-----------------+-------+-------+--------+------------+



.. |br| raw:: html

   <br />
