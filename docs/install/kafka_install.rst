***********************
Lago Kafka Installation
***********************

**If access to a Kafka cluster already exits, skip to the**
`next section <./producers_install_kafka.html>`_

---------------------------

With a K8s cluster in place, whether using the `K3s implementation <./k8s_install.html>`_  or an existing cluster,
install the `Strimzi Kafka Operator <https://strimzi.io/>`_



**ON THE ADMIN SYSTEM**

**1.) Check to make sure that KUBECONFIG is set:**

    .. code::

        $ echo $KUBECONFIG

        /home/lagouser/.kube/configs/tbd-lago

    **if the above is empty or not what it should be, sue the following to set it correctly**

    .. code:: bash

        export KUBECONFIG=<path>/kubeconfig

**2.) Add the Strimzi chart repository**

    .. code:: bash

        helm repo add strimzi https://strimzi.io/charts/

**3.) Install the Kafka Operator on the cluster**

    .. code:: bash

        helm install kafka-operator strimzi/strimzi-kafka-operator -n pensando --create-namespace

The Kafka operator is now installed. It is *not* running, that comes later in the `Lago Producers install section <./producers_install.html>`_
