***************************
Kubernetes Cluster Creation
***************************

**If there is an existing Kubernetes cluster Kafka and/or the Lago Producers can be deployed to, skip to the**
`next section <./kafka_install.html>`_

---------------------------

Lago has differing T-shirt installation values files with installation values for a small, single system cluster
(great for quick demos and simple testing) or a large, three system cluster [1]_ that works well for a PoC or even in a
production capacity.

Below are the instructions for a Ubuntu 18.04 (or later) cluster of 3 machines. Pare it down to a single server for a
demo/dev cluster.

To expedite the process of installing k8s, these instructions use `Rancher K3s <https://rancher.com/docs/k3s/latest/en/>`_
which is just a binary form of k8s that is self contained.  Works the same but has a smaller footprint and is easy to
install.

.. note::
 It is suggested to admin the k8s cluster from a system that is *not* part of the cluster, i.e. a laptop, a different VM,
 etc. The instructions are written as if installation is occuring from a \*nix system outside of the cluster.


**ON THE ADMIN SYSTEM**

  **1.) Generate ssh keys**

  For the installer to work correctly, it needs to connect via ssh to each node
  in the cluster.  Easiest way to do this is to create an ssh-key pair on the admin
  system and then push it to each node that will be in the cluster.

  .. code:: bash

    ssh-keygen -f ~/.ssh/pen-key-ecdsa -t ecdsa -b 521

  **2.) Copy ssh keys**

  Copy the ssh key to each node that will be in the
  cluster and test the connection as it instructs after copying the key.

  .. code:: bash

    ssh-copy-id -i ~/.ssh/pen-key-ecdsa <user>@<ip of node>

  **3.)** `Install kubectl for admin server OS <https://v1-18.docs.kubernetes.io/docs/tasks/tools/install-kubectl/>`_

  **4.)** `Install helm for admin server OS <https://helm.sh/docs/intro/install/#through-package-managers>`_

---------------------------

|br|

**ON EACH NODE IN THE K8S CLUSTER**


  **1.) Turn off swap**

  Kafka doesn't work too well with swap, so we will just get rid of it.

  .. code:: bash

    sudo swapoff --all
    sudo sed -ri '/\sswap\s/s/^#?/#/' /etc/fstab


  **2.) Add passwordless sudo for the user** (this is for the install to work)

    .. code:: bash

      whoami

    The output of the above command is the username to add to the sudoers file below

    .. code:: bash

      sudo visudo


    **add the following to the end of the file:**

      <username from whoami command> ALL=(ALL) NOPASSWD: ALL

  **3.) Logout of server**

---------------------------

  |br|

**ON THE ADMIN SYSTEM**

  **1.) Install** `k3sup <https://github.com/alexellis/k3sup>`_ (pronounced "ketchup")

    .. code:: bash

      curl -sLS https://get.k3sup.dev | sh
      sudo install k3sup /usr/local/bin/
      k3sup --help

  **2.) Download** `bootstrap.sh <https://gitlab.com/pensando/tbd/project-lago/lago-docs/-/raw/master/docs/_static/bootstrap.sh>`_
    Edit the USER, IPs, number of servers and the ssh-key filename and save


  **3.) Run k3s install**

    .. code:: bash

      sh ./bootstrap.sh

    This will install `Rancher K3s <https://rancher.com/docs/k3s/latest/en/>`_ to each server defined in the
    bootstrap.sh file

    This will generate a kubeconfig file in the home directory of the admin system.  This file can moved, but is
    needed to instruct kubectl and helm how to access the K8s cluster

  **4.) Set KUBECONFIG env var**

      .. code::

        export KUBECONFIG=$HOME/kubeconfig

  **5.) Validate cluster is running**

  .. code:: bash

      kubectl get nodes
      NAME       STATUS   ROLES         AGE    VERSION
      penkube1   Ready    etcd,master   105s   v1.19.5+k3s2
      penkube2   Ready    etcd,master   76s    v1.19.5+k3s2
      penkube3   Ready    etcd,master   47s    v1.19.5+k3s2

---------------------------

.. note::

  At this point there should be a funcitoning cluster.  If it is not running, run through the above again and verify
  that everything is set up correctly.  If that still doesn't work, send email `here <incoming+pensando-tbd-project-lago-lago-docs-23835515-issue-@incoming.gitlab.com>`_
  and it will open an issue in Gitlab for the documentation and someone will get back to you asap.

|br|

If Kafka is needed proceed to `install Kafka <./kafka_install.html>`_
If Kafka is already available proceed to  `install the Pensando Producers with an existing Kafka cluster <./producers_install_kafka.html>`_

|br|

.. [1]  This will handle 30K DSCs and 30 PSMs without a problem.  You're welcome Pez.


.. |br| raw:: html

   <br />
