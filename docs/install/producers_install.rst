********************************************
Lago Producers Installation with Lago Kafka
********************************************

With a K8s cluster, whether using the K3s implementation or an existing cluster, proceed to install the Pensando Lago
producers via helm.

Download and edit a `values file <./values_explanation.html>`_ that will configure Lago.
Below are 3 examples.  Cut and paste the text into a file on the admin server. These values files follow the tables in
the `Compute Servers <./compute.html>`_ section which, hopefully, was used to size the install. If not, well, good luck.....

| **Example values files:**
|   `Single Node K8s Cluster <https://gitlab.com/pensando/tbd/project-lago/lago-helm/-/raw/master/values-files/values-Small.yaml>`_:
|      Testing, demos and small PoCs with < 10 DSCs involved
|   `3 Node K8s Cluster <https://gitlab.com/pensando/tbd/project-lago/lago-helm/-/raw/master/values-files/values-Large.yaml>`_ :
|      PoCs with more than a few dozen DSCs
|   `3 Node K8s Cluster with Prometheus Monitoring <https://gitlab.com/pensando/tbd/project-lago/lago-helm/-/raw/master/values-files/values-example-monitoring.yaml>`_  :
|      PoCs with a monitoring requirement and/or production

.. note::
    For the Prometheus monitoring values file, there must be a separate Prometheus setup.  It will still install but
    without having Prometheus somewhere, and configured to gather data from the K8s cluster, it won't do much good.



| **Editing the values files:**
| Once the values file is downloaded, open in an editor and find the entries for ipfix-ingest, syslog-ingest and psm-collect.

:ref:`IPFIX_INGEST` :

| **Editable Items:**
|    *config -> cloudevent*       (optional)
|       If set to true, this will wrap the topic entry in a standard `CloudEvents <https://cloudevents.io/>`_ header
|    *forwarding -> enabled*      (optional)
|       By default this is false.  If set to true, for every IPFix packet received, the producer will write it to the
|       topic *AND* will also replicate it (as is) to the collector defined with the values for addr and port.

======

:ref:`SYSLOG_INGEST` :

| **Editable Items:**
|    *config -> cloudevent*       (optional)
|       If set to true, this will wrap the topic entry in a standard `CloudEvents <https://cloudevents.io/>`_ header

======

:ref:`PSM_COLLECT` :

| **Editable Items:**
|    *config -> poll*        (optional)
|       Interval for how often the producer talks to each PSM defined and running APIs for DSC, Workload and Cluster inventory
|    *config -> metrics*     (optional)
|       Interval for how often the producer talks to each PSM defined and running APIs for retrieving metrics
|    *psm*                   (required)
|       Address, username and password for each PSM server that Lago is receiving IPFix and/or syslog from DSCs and which
|       psm-collect will run APIs against (hence the need for a login)

======


With the values file edited to the correct configuration:

**ON THE ADMIN SYSTEM**

**1.) Create Docker Image Pull Secrets**  (this is all one command)

    .. code:: bash

        kubectl create secret docker-registry pensando-image-creds \
        --docker-server=registry.gitlab.com \
        --docker-username=gitlab+deploy-token-349500 \
        --docker-password=sufyMzq4GAaebsLjcBYU --docker-email=deploy@pensando.io

**2.) Add the Lago producer helm charts**

    .. code:: bash

        helm repo add pensando https://pensando.gitlab.io/tbd/project-lago/lago-helm

| **3.) Install Lago**
|   Note in the below command to insert the name of the values file downloaded and configured.

    .. code:: bash

        helm install lago pensando/lago -f <values file from above> -n pensando --create-namespace


| There should now be a fully functioning Lago setup - checked by running the *kubectl get pods* command:
| (the output of the following command may differ slightly from system to system and depending on the values config)

.. code:: bash

    $ kubectl get pods

    NAME                                                     READY   STATUS    RESTARTS   AGE
    alertmanager-monitoring-kube-prometheus-alertmanager-0   2/2     Running   0          5d12h
    ingress-nginx-controller-5ff9dc4fd-gd5md                 1/1     Running   0          5d12h
    lago-ipfix-ingest-548d75dd4-f7584                        2/2     Running   0          5d12h
    lago-ipfix-ingest-548d75dd4-qlhh8                        2/2     Running   0          14d
    lago-ipfix-ingest-548d75dd4-zwhp5                        2/2     Running   0          14d
    lago-psm-collect-56c658878c-qfhqh                        1/1     Running   0          13d
    lago-syslog-ingest-57b8565cfd-95lvv                      1/1     Running   0          13d
    lago-syslog-ingest-57b8565cfd-ckf6f                      1/1     Running   0          14d
    lago-syslog-ingest-57b8565cfd-d4l5z                      1/1     Running   0          5d12h
    monitoring-grafana-76f45d54bc-28xpm                      2/2     Running   0          57d
    monitoring-kube-prometheus-operator-cd6bc5ddf-bcrww      1/1     Running   0          44d
    monitoring-kube-state-metrics-6bfb865c69-jk8ks           1/1     Running   0          44d
    monitoring-prometheus-node-exporter-vf6wm                1/1     Running   0          78d
    monitoring-prometheus-node-exporter-w8q87                1/1     Running   0          78d
    monitoring-prometheus-node-exporter-wzt29                1/1     Running   0          78d
    pensando-cluster-entity-operator-5bcdc88ddb-522cp        2/2     Running   0          79d
    pensando-cluster-kafka-0                                 1/1     Running   0          45d
    pensando-cluster-kafka-1                                 1/1     Running   0          79d
    pensando-cluster-kafka-2                                 1/1     Running   0          5d12h
    pensando-cluster-zookeeper-0                             1/1     Running   0          45d
    pensando-cluster-zookeeper-1                             1/1     Running   0          79d
    pensando-cluster-zookeeper-2                             1/1     Running   0          5d12h
    prometheus-monitoring-kube-prometheus-prometheus-0       2/2     Running   1          5d12h
    strimzi-cluster-operator-68c6747bc6-zq9vl                1/1     Running   0          44d
    svclb-ingress-nginx-controller-gw46m                     3/3     Running   0          14d
    svclb-ingress-nginx-controller-lxhvr                     3/3     Running   0          14d
    svclb-ingress-nginx-controller-xvfg4                     3/3     Running   1          14d


Once the output for the kubectl get pods command looks similar to the above, proceed to the `Configure PSM and DSCs
section <./psm_dsc.html>`_ to finalize the Lago installation.

.. |br| raw:: html

   <br />
