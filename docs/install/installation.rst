************
Installation
************

Before you get started, you need to select the proper link below and it will take
you to the point in the installation instructions based on what you already have
built in your infrastructure.  Lago is built with modularity so you only have to
install that which you don't have, not everything.

.. toctree::
    :maxdepth: 2

    compute
    k8s_install
    kafka_install
    producers_install
    producers_install_kafka
    psm_dsc
.. - `Kubernetes installation <./k8s_install.rst>`_
.. - [I have a K8s cluster, but I don't have Kafka](kafka_install.md)
.. - [I have both K8s and Kafka but need the Lago producers](producers_install.md)
