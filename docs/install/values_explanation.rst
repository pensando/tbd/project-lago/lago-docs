*****************************
Why do I need a values file?
*****************************

A values file is a plain yaml file that is used by Helm to set configuration parameters for the chart you are installing.

By default, a Helm chart has it's own values that are contained in the chart itself. These are superseded if you specify
a values file on the cmd line while installing, using the -f <values file> parameter.  The values file can even be
superseded by using cmd line parameters, using the --set <key=value>, when installing the chart.

As an example, let's say you have a helm chart for Elasticsearch that comes with a parameter that specifies using 2GB
RAM for each replica of the Elasticsearch DB you install.  This is the default BTW if you use the Elastic helm charts
available `here <https://github.com/elastic/helm-charts/blob/master/elasticsearch/values.yaml>`_

<snip>

.. code-block:: yaml

    resources:
        requests:
            cpu: "1000m"
            memory: "2Gi"
        limits:
            cpu: "1000m"
            memory: "2Gi"

<snip>

Well, you know that you are going to need more than that and you have the resources, so you bump it up to 4GB and this
is the only thing you change.

Here would be the entire values.yaml file that you will send to helm using the -f flag:

.. code-block:: yaml

    ---
    clusterName: "elasticsearch"
    resources:
        requests:
            cpu: "1000m"
            memory: "4Gi"
        limits:
            cpu: "1000m"
            memory: "4Gi"

As you can see, you are overriding the default 2GB setup in the helm chart itself.

This is a simple example.  For Lago, we use/change a lot of different parameters depending on your setup.  We provide
examples and you can manipulate those examples to fit your scenario.

`Back to Lago Producers Installation <./producers_install.html>`_
