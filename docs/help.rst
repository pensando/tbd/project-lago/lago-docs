****
Help
****

If you find an issue or need more help, you can open an issue in the corresponding repository:
|br|


.. list-table:: Open an Issue
   :widths: 25 25 50
   :header-rows: 1

   * - Repository
     - Click One
     - Description
   * - Lago Helm Charts
     - `Website <https://gitlab.com/pensando/tbd/project-lago/lago-helm/-/issues/new>`_ or via `email <incoming+pensando-tbd-project-lago-lago-helm-24155105-issue-@incoming.gitlab.com>`_
     - If you are having issues with installing |br| the helm charts or understanding what |br| is happening when you do install them.
   * - Lago Stream Processors
     - `Website <https://gitlab.com/pensando/tbd/project-lago/lago-stream-processor/-/issues/new>`_ or via `email <incoming+pensando-tbd-project-lago-lago-stream-processor-24114527-issue-@incoming.gitlab.com>`_
     - If everything is installed but either you |br| are not getting info into Kafka or it |br| looks like the wrong info coming out.
   * - Lago Docs & Misc
     - `Website <https://gitlab.com/pensando/tbd/project-lago/lago-docs/-/issues/new>`_ or via `email <incoming+pensando-tbd-project-lago-lago-docs-23835515-issue-@incoming.gitlab.com>`_
     - Anything you find in this documentation that |br| needs fixing or have any general |br| questions about Lago itself.


.. |br| raw:: html

   <br />
