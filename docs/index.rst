.. project-lago documentation master file, created by
   sphinx-quickstart on Fri Mar 19 11:45:11 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

***************************
Project Lago Documentation
***************************

.. toctree::
   :caption: Project Lago
   :maxdepth: 4

   overview
   arch/architecture
   install/installation
   help


.. toctree::
   :caption: Operations
   :maxdepth: 3

   kafka/consumers/index
   kafka/producers/index



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. |br| raw:: html

   <br />

.. role:: underline
    :class: underline