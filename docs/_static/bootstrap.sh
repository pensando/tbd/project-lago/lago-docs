#!/bin/bash
set -e
export NODE_1="10.29.75.161"
export NODE_2="10.29.75.162"
export NODE_3="10.29.75.163"

# Username of user on K8s node that will be doing the install (whoami output)
export USER=<username>

# The first server starts the cluster - this must run
k3sup install \
  --cluster \
  --user $USER \
  --ip $NODE_1 \
  --ssh-key ~/.ssh/id_rsa_pen \
  --k3s-version v1.19.5+k3s2

#The second node joins - comment out if you only have 1 node
k3sup join \
  --server \
  --ip $NODE_2 \
  --user $USER \
  --server-user $USER \
  --server-ip $NODE_1 \
  --ssh-key ~/.ssh/id_rsa_pen \
  --k3s-version v1.19.5+k3s2

# The third node joins - comment out if you don't have 3 nodes
k3sup join \
  --server \
  --ip $NODE_3 \
  --user $USER \
  --server-user $USER \
  --server-ip $NODE_1 \
  --ssh-key ~/.ssh/id_rsa_pen \
  --k3s-version v1.19.5+k3s2
