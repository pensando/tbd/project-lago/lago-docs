
************
Architecture
************

Lago, at it's core, is an implementation of Kubernetes, Kafka and Kafka prodcuers provided by Pensando Systems.

.. image:: ../_static/img/highlvlarch.png

The Lago K8s Architecture

|br|

Linux Compute
---------------

At a minimum there will need to be 1 Linux machine.  The lone Linux machine can be any OS, but all of the
examples throughout this doc are for a Ubuntu system (18.04 or newer) and that is what the Pensando DevOps team uses for
90%+ of k8s implementations. It is highly suggested to stick with Ubuntu unless you are familiar with
Kubernetes on your particular choice of OS.  This has also been tested on Mac and WSL2 laptops, so that is a viable
install option for kicking the tires.

The table `located here <../install/compute.html>`_ shows the minimum requirements for number of DSCs/PSMs that are used
in your deployment of the Pensando Platform.  These requirements will allow for deployment of the Lago producers and the
Kafka cluster indicated.

|br|

Kubernetes
--------------------

The foundation of Lago is built on Kubernetes.  This is the simplest way to provide an IaC structure to deploying the
microservices.  Offering easier orchestartion and scale, and allows for Lago to be built on-prem in a data center or on
any cloud provider.  Most cloud providers even have wizards for building k8s clusters automatically, so that is an
option.

If you already have a K8s cluster that can handle the producers, and Kafka if needed - see Kafka section below - verify
the compute `requirements <../install/compute.html>`_ and make sure you have the resources needed.

If you don't already have a K8s cluster, there is a bootstrap script (provided in the install section) that will install
Rancher K3s. This bootstrap script can be use to set up a 1 to N master-node cluster with embedded etcd for
redundancy.

|br|

Helm
--------------------

Helm charts are provided via a VCS and utilized to configure the intended deployment and make it easy to manually or
programmatically implement Lago on a K8s cluster.

| The Helm charts consist of 2 chart repositories.
|   1. `Strimzi Kafka on Kubernetes <https://strimzi.io/>`_
|   2. Pensando Stream Processors

|br|

Kafka
--------------------

 All of the parts of Lago were built/configured to get data into Kafka, which is the main impetuous for building Lago in
 the first place.

 The Strimzi Kafka operator is used for a quick, configurable deployment of Kafka without having to know how to install
 Kafka, any of its supporting pkgs and performs automated operations on the deployed pods so the end-user doesn't need
 to be a Kafka administrator to use it.  That being said, changes related to the Kafka implementation are not
 part of Lago or any of these instructions.  If there is an existing Kafka cluster that can be used, this operator is
 not needed and the installation can be skipped.  There are two sets of install instructions for the Lago helm charts,
 each relating to the built-in Kafka and for an existing Kafka cluster, respectively.

|br|

Kafka Producers
--------------------

The Producer nodes are defined in helm charts by Pensando Systems.  These charts deploy and auto-scale the
following functional aspects of Lago:

.. _SYSLOG_INGEST:

syslog-ingest
*************
The syslog-ingest pod is a Pensando built stream processor that accepts RFC5424 syslog from DSCs and
RFC5424 syslog (alerts/events) from PSM. Each replica opens a TCP/UDP raw socket that accepts syslog packets on the
configured port.  It is also comes with a K8s service that provides load balancing across all pods deployed within the
deployment and a K8s nodeport to abstract the service onto a host port.

If the syslog is from the DSC and a flow log, it parses the message, enriches information from the message with DSC
specific information retrieved from PSM (see :ref:`PSM_COLLECT`) and then places the info onto the topic as a JSON
document.

If the syslog is from the PSM (events/alerts), the message is parsed into JSON and placed on the topic.

Refer to the Pensando Users Guide on how to configure the DSCs and PSM(s) to forward RFC5424 to your syslog-ingest
collectors in Lago.

| **The default ports for syslog-ingest targets are:**
| Flowlog from DSCs: UDP/30005
| Events/Alerts from PSM: UDP/30006

.. image:: ../_static/img/SyslogIngest.png
     :width: 600


.. _IPFIX_INGEST:

ipfix-ingest
************

The ipfix-ingest pod is based on the `vFlow <https://github.com/VerizonDigital/vflow>`_ collector built by Verizon.  It
also contains a Pensando created container that does processing before the entry is written to the topic.  The helm
chart includes a K8s service that provides load balancing across all ipfix replicas deployed within the deployment and a
K8s nodeport to abstract the service onto a host port.

If configured, the pod can also replicate and forward the raw IPFix to a different collector, as well as placing the
IPFix data on the topic, as JSON.  This is done in the config:forwarding section of the
`values file <./install/values_explanation.html>`_   when you install/update/upgrade Lago using Helm.  This is
convenient if you already have a collector, but would also like to have a history of your IPFix data or have others that
would consume the data programmatically.

Refer to the Pensando Users Guide on how to configure the DSCs to forward IPFix to your ipfix-ingest
collectors in Lago.

| **The default port for an ipfix-ingest target is:**
| IPFix from DSCs: UDP/30739

.. image:: ../_static/img/IPFixIngest.png
     :width: 600

.. _PSM_COLLECT:

psm-collect
************

The psm-collect pod is a Pensando built producer that retrieves, via the PSM REST API, inventory information about DSCs,
Workloads, PSM Cluster information and DSC Metrics.  Each of these API calls is on a configurable interval, as explained
below.  All PSMs that manage the DSCs that are generating IPFix and/or syslog data to Lago must be included in the psm
section of the `values file <./install/values_explanation.html>`_ or Lago will not be able to enrich topics correctly
and this will be rather useless.

Each API call for *DSCs, Workloads and Cluster Info* places the JSON retrieved onto a keyed topic.  This means that each
entry on the topic has a key (the DSC topic has the DSC name as an example) and each time the info is written to the
topic, the key is used and the data in that keyed part of the topic is overwritten.  This allows for only the latest
information to be present in the topic at any given time.  This is more of an inventory of DSCs, Workloads and PSM
Clusters and should be treated as such, rather than as a rolling log of event and message data.

Each API call for *Metrics* places the returned JSON onto the topic as it is received. There is no key and this data
should be treated as a log of metrics information that is continuosly updated.

Each psm-collect replica is an implementation of PSM REST API calls that retrieves Workflows, DSCs and Metrics.  Each of
these is configured to call all PSM nodes defined in the `values file <./install/values_explanation.html>`_ at an
interval of 30 seconds by default. You can change these intervals by adjusting the poll and metrics sections of the
values file when you install/update/upgrade Lago using Helm.


.. |br| raw:: html

   <br />
